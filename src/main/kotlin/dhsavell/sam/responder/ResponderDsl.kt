package dhsavell.sam.responder

import dhsavell.sam.SamBot
import sx.blah.discord.handle.obj.IMessage

@DslMarker
annotation class ResponderDsl

/**
 * A closure-based Responder implementation, used in the Responder DSL.
 *
 * @param name Name of the responder
 * @param triggers An array of ResponseInvokers determining what should trigger this response
 * @param action The action that should be executed when this response is triggered
 */
private class ClosureBasedResponder(
    override val name: String,
    override val triggers: List<ResponseInvoker>,
    private val action: (SamBot, IMessage) -> Unit
) : Responder {
    override fun respond(bot: SamBot, context: IMessage) = action(bot, context)
}

/**
 * DSL builder for fluently creating a Responder.
 */
@ResponderDsl
class ResponderBuilder {
    private var name = "Unnamed Responder"
    private var responseInvokers = emptyList<ResponseInvoker>()
    private var action: (SamBot, IMessage) -> Unit = { _, _ -> }

    /**
     * Sets the name of this Response.
     */
    fun name(init: () -> String) {
        name = init()
    }

    /**
     * Defines what activates this Response.
     * @see ResponseInvokerListBuilder for specific syntax
     */
    fun respondsTo(init: ResponseInvokerListBuilder.() -> Unit) {
        responseInvokers += ResponseInvokerListBuilder().apply(init).build()
    }

    /**
     * Executes the given action when this response is triggered.
     */
    fun whenActivated(init: (SamBot, IMessage) -> Unit) {
        action = init
    }

    /**
     * Sends the resulting String as a message when this response is triggered.
     */
    fun answersWith(init: () -> String) {
        whenActivated { _, message ->
            message.channel.sendMessage(init())
        }
    }

    /**
     * Builds this Responder.
     */
    fun build(): Responder = ClosureBasedResponder(name, responseInvokers, action)
}

/**
 * DSL builder for fluently creating a list of ResponseInvokers.
 * @see ResponderBuilder for where this is used
 */
@ResponderDsl
class ResponseInvokerListBuilder {
    private var responseInvokers: List<ResponseInvoker> = emptyList()

    /**
     * Creates a ResponseInvoker by partially applying a PositionalPattern with the target being the current String.
     * @see PositionalPattern for an overview of positional patterns
     */
    infix fun String.at(pattern: PositionalPattern) {
        responseInvokers += { message: IMessage ->
            pattern.containsMatch(
                message.content.toLowerCase(),
                this.toLowerCase()
            )
        }
    }

    /**
     * Creates a ResponseInvoker from a given predicate.
     */
    fun condition(invoker: ResponseInvoker) {
        responseInvokers += invoker
    }

    fun build(): List<ResponseInvoker> = responseInvokers
}

/**
 * Primary DSL method for building a Responder.
 * @see ResponderBuilder for DSL methods
 */
fun newResponder(init: ResponderBuilder.() -> Unit): Responder = ResponderBuilder().apply(init).build()