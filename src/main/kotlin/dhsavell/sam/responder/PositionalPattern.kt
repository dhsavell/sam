package dhsavell.sam.responder

/**
 * An interface for determining whether a given target String exists in a certain position of a "check" String
 */
interface PositionalPattern {

    /**
     * Determines whether or not a target string exists at a certain position in a provided "check" string.
     * @param check String to search in
     * @param target String to look for in the check string
     */
    fun containsMatch(check: String, target: String): Boolean
}

/**
 * Matches target words at the beginning of a string.
 */
object Beginning : PositionalPattern {
    override fun containsMatch(check: String, target: String): Boolean {
        return check == target || check.startsWith("$target ")
    }
}

/**
 * Matches target words anywhere within a string.
 */
object Anywhere : PositionalPattern {
    override fun containsMatch(check: String, target: String): Boolean {
        return check == target || check.containsWord(target)
    }
}

/**
 * Matches target words at the end of a string.
 */
object End : PositionalPattern {
    override fun containsMatch(check: String, target: String): Boolean {
        return check == target || check.endsWith(" $target")
    }
}

/**
 * Determines whether or not this string contains a given word (a target string separated by spaces)
 */
fun String.containsWord(word: String): Boolean {
    return contains(" $word ") || startsWith("$word ") || endsWith(" $word")
}